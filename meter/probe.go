package meter

import (
	"fmt"
	"github.com/rcrowley/go-metrics"
	"log"
	"net/http"
	"time"
)


var registry = metrics.NewRegistry()
var defaultClient = http.Client{
	Timeout: 5 * time.Second,
}

const requests = 10

func ProbeHTTPEndpoint(host string, port int) {
	timer := metrics.GetOrRegisterTimer(fmt.Sprintf("%s", host), registry)

	for i := 0; i < requests; i++ {
		timer.Time(func() {
			url := fmt.Sprintf("%s://%s/", proto(port), host)
			get, err := defaultClient.Get(url)

			if err != nil {
				metrics.GetOrRegisterCounter(fmt.Sprint(url," ??? status code"), registry).Inc(1)
			} else {
				metrics.GetOrRegisterCounter(fmt.Sprint(url, " ", get.StatusCode, " status code"), registry).Inc(1)
			}

		})
	}
}

func proto(port int) string {
	if port == 443 {
		return "https"
	} else {
		return "http"
	}
}

func StartReport() {
	go RefreshEvery(15*time.Second, reportMeanRate)
}

func reportMeanRate() {
	registry.Each(func(name string, meter interface{}) {
		switch m := meter.(type) {
		case metrics.Counter:
			// meh
		case metrics.Timer:
			log.Printf("[%40s] mean: %12.2fms, p95: %12.2fms ", name, m.Mean() / float64(time.Millisecond), m.Percentile(0.95)/ float64(time.Millisecond))
		}
	})
}

func RefreshEvery(d time.Duration, function func()) {
	for range time.Tick(d) {
		function()
	}
}