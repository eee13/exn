package geo

import (
	"testing"
)

func TestParse(t *testing.T) {
	response := `<?xml version="1.0" encoding="iso-8859-1"?>

<ip>
<results><result><ip>78.140.182.250</ip><host>78.140.182.250</host><isp>Webzilla B.V.</isp><city>SomeCity</city><countrycode>NL</countrycode><countryname>Netherlands</countryname><latitude>52.3824</latitude><longitude>4.8995</longitude></result></results>
</ip>`
	gr, err := Parse([]byte(response))

	if err != nil {
		t.Fatalf("%+v", err)
	}
	res := gr.Results.Result

	assertEquals( res.Ip, "78.140.182.250", t)
	assertEquals( res.Host, "78.140.182.250", t)
	assertEquals( res.Isp, "Webzilla B.V.", t)
	assertEquals( res.City, "SomeCity", t)
	assertEquals( res.CountryCode, "NL", t)
	assertEquals( res.CountryName, "Netherlands", t)
	// ...
}

func TestResolve(t *testing.T) {
	ip := "78.140.182.250"
	resolve, err := Resolve(ip)
	if err != nil {
		t.Fatalf("%+v", err)
	}

	assertEquals(resolve.Results.Result.Ip, ip, t)
}

func assertEquals(this, that string, t *testing.T) {
	if this != that {
		t.Fatalf("%s not equals %s", this, that)
	}
}
