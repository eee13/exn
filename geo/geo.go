package geo

import (
	"bytes"
	"encoding/xml"
	"io"
	"io/ioutil"
	"net/http"
)

type GeoIpResponse struct {
	XMLName xml.Name `xml:"ip"`
	Results struct {
		Result struct {
			Ip string `xml:"ip"`
			Host string `xml:"host"`
			Isp string `xml:"isp"`
			City string `xml:"city"`
			CountryCode string `xml:"countrycode"`
			CountryName string `xml:"countryname"`
			Latitude string `xml:"latitude"`
			Longitude string `xml:"longitude"`
		}  `xml:"result"`
	}  `xml:"results"`
}


var baseUrl = "http://api.geoiplookup.net/?query="

func Resolve(ip string) (*GeoIpResponse, error) {
	response, err := http.Get(baseUrl + ip)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return Parse(body)
}

func noopDecoder(encoding string, input io.Reader) (io.Reader, error) {
	return input, nil
}

func Parse(body []byte) (*GeoIpResponse, error) {
	var response GeoIpResponse
	reader := bytes.NewReader(body)
	decoder := xml.NewDecoder(reader)
	decoder.CharsetReader = noopDecoder
	err := decoder.Decode(&response)

	return &response, err
}