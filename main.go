package main

import (
	"exn/dns"
	"exn/geo"
	"exn/meter"
	"exn/ports"
	"exn/subdomains"
	"fmt"
	"log"
	"time"
)

func main() {
	sds, err := subdomains.FindSubdomains("exness.com")
	if err != nil {
		log.Fatalf("Couldn't find subdomains: %s ", err)
	}

	log.Println("Total", len(sds), "of (sub)domains found:")
	var httpEndpoints = map[string]int{}
	for _, sd := range sds {
		ip := dns.ResolveIP(sd)
		if ip == "" {
			log.Printf("DNS lookup failed, skipping [%s]", sd)
			continue
		}
		result, err := geo.Resolve(ip)
		if err == nil {
			geoInfo := result.Results.Result
			log.Printf("+ %s [%s], country: %s (%s)\n", sd, ip, geoInfo.CountryName, geoInfo.CountryCode )
		} else {
			log.Printf("+ %s [%s], country: ? (%s)\n", sd, ip, err.Error())
		}
		openPorts := ports.Scan(ip)

		for port := range openPorts {
			if ports.IsHTTP(port) {
				httpEndpoints[fmt.Sprintf("%s:%d", sd, port)] = port
			}
			log.Println("+ + ", port, "/", ports.DescriptionTCP(port), "is open")
		}
		log.Println()
	}
	log.Println("Discovered", len(httpEndpoints),"http endpoints. Probing:")


	meter.StartReport()
	for {
		for host, port := range httpEndpoints {
			meter.ProbeHTTPEndpoint(host, port)
		}
		time.Sleep(5 * time.Second)
	}
}


