package ports

import (
	"net"
	"strconv"
	"sync"
	"time"
)

const parallelism = 30

var mutex = sync.Mutex{}

type scanTask struct {
	Proto string
	Ip    string
	Port  int
}

type scanResult struct {
	scanTask
	Status bool
}

var input = make(chan scanTask, 10000)
var output = make(chan scanResult, 10000)

func worker(i chan scanTask, o chan scanResult) {
	for t := range i {
		status := isOpen(t.Proto, t.Ip, t.Port)
		o <- scanResult{
			scanTask: scanTask{
				Proto: t.Proto,
				Ip:    t.Ip,
				Port:  t.Port,
			},
			Status: status,
		}
	}
}

func init() {
	for i := 0; i < parallelism; i++ {
		go worker(input, output)
	}
}

func Scan(ip string) (openTcp map[int]struct{}) {
	mutex.Lock()
	defer mutex.Unlock()

	//region TCP

	openTcp = map[int]struct{}{}
	results := 0
	submitted := 0
	for port, _ := range TopTcpPorts() {
		submitted++
		input <- scanTask{
			Proto: "tcp",
			Ip:    ip,
			Port:  port,
		}
	}
	for results < submitted {
		res := <-output
		results++
		if res.Status {
			openTcp[res.Port] = struct{}{}
		}
	}
	//endregion

	return
}

func isOpen(protocol, hostname string, port int) bool {
	address := hostname + ":" + strconv.Itoa(port)
	conn, err := net.DialTimeout(protocol, address, 10*time.Second)
	if err != nil {
		return false
	}
	defer conn.Close()
	return true
}
