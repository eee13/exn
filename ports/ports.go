package ports

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
)


var tcp = map[int]string{}
var udp = map[int]string{}

func init() {
	// https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml
	csvFile, err := os.Open("./ports/service-names-port-numbers.csv")
	if err != nil {
		panic(err)
	}
	defer csvFile.Close()
	records, err := csv.NewReader(csvFile).ReadAll()

	for  _, record := range records {
		if record[1]  == "" || record[0] == ""{
			/* meh */
			continue
		}
		portNo, _ := strconv.Atoi(record[1])
		if record[2] == "tcp" {
			tcp[portNo] = record[0]
		}
		if record[2] == "udp" {
			udp[portNo] = record[0]
		}
	}

	log.Println("Known TCP ports:", len(tcp))
	log.Println("Known UDP ports:", len(udp))
}

func IsHTTP(port int) bool {
	return port == 80 || port == 443 || port == 8080 // ...
}

func DescriptionTCP(port int) string {
	return tcp[port]
}