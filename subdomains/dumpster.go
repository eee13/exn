package subdomains

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

var csrfRegex = regexp.MustCompile(`(?s)<input type="hidden" name="csrfmiddlewaretoken" value="(\w*)">`)

var tableRegex = regexp.MustCompile(`(?s)<a name="hostanchor"></a>Host Records.*?<table.*?>(.*?)</table>`)
var linkRegex = regexp.MustCompile(`(?s)<td class="col-md-4">(.*?)<br>`)

func getCsrf(body string) (string, error) {
	if !csrfRegex.MatchString(body) {
		return "", errors.New("no match")
	}
	csrf := csrfRegex.FindStringSubmatch(body)[1]
	return csrf, nil
}

func getSubdomains(body string) ([]string, error) {
	if !tableRegex.MatchString(body) {
		return []string{}, errors.New("no match")
	}

	table := tableRegex.FindStringSubmatch(body)[1]

	var res []string
	for _, groups := range linkRegex.FindAllStringSubmatch(table, -1) {
		res = append(res, groups[1])
	}

	return res, nil
}

const baseUrl = "https://dnsdumpster.com"

func FindSubdomains(domain string) ([]string, error) {
	firstPageReader, err := http.Get(baseUrl)
	if err != nil {
		return []string{}, err
	}
	csrf, err := getCsrf(bodyAsString(firstPageReader))
	if err != nil {
		return []string{}, err
	}

	params := url.Values{}
	params.Set("csrfmiddlewaretoken", csrf)
	params.Set("targetip", domain)
	params.Set("user", "free")

	req, err := http.NewRequest("POST", baseUrl, strings.NewReader(params.Encode()))
	if err != nil {
		panic("What? err:" + err.Error())
	}
	req.Header.Set("Cookie", "csrftoken="+csrf)
	req.Header.Set("Referer", baseUrl)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Origin", baseUrl)

	client := &http.Client{}
	secondPageReader, err := client.Do(req)

	if err != nil {
		return []string{}, err
	}

	asString := bodyAsString(secondPageReader)
	return getSubdomains(asString)
}

func bodyAsString(r *http.Response) string {
	defer r.Body.Close()
	bytes, _ := ioutil.ReadAll(r.Body)
	return string(bytes)
}
