package dns

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net"
	"net/http"
)

type DnsJson struct {
	Status   int  `json:"Status"`
	TC       bool `json:"TC"`
	RD       bool `json:"RD"`
	RA       bool `json:"RA"`
	AD       bool `json:"AD"`
	CD       bool `json:"CD"`
	Question []struct {
		Name string `json:"name"`
		Type int    `json:"type"`
	} `json:"Question"`
	Answer []struct {
		Name string `json:"name"`
		Type int    `json:"type"`
		TTL  int    `json:"TTL"`
		Data string `json:"data"`
	} `json:"Answer"`
}

func ResolveIP(name string) string {
	req, err := http.NewRequest("GET", "https://cloudflare-dns.com/dns-query", nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Accept", "application/dns-json")
	q := req.URL.Query()
	q.Add("name", name)

	req.URL.RawQuery = q.Encode()

	client := &http.Client{}

	if err != nil {
		log.Fatal(err)
	}
	res, err := client.Do(req)
	if err != nil {
		log.Println("Cloudflare resolver failed for name:", name, " with err: ", err)
		log.Println("Falling back to local one")
		fb, err := net.LookupAddr(name)
		if err != nil {
			log.Println("Fallback failed as well. Err:", err)
			return ""
		}
		return fb[0]
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	var dnsResult DnsJson
	if err := json.Unmarshal(body, &dnsResult); err != nil {
		log.Fatal(err)
	}

	return dnsResult.Answer[0].Data
	//for _, dnsAnswer := range dnsResult.Answer {
	//	fmt.Println("IP:", dnsAnswer.Data)
	//}
}
